locals {
  # Sort environment variables so terraform will not try to recreate on each plan/apply
  env_vars             = var.environment != null ? var.environment : []
  env_vars_keys        = var.map_environment != null ? keys(var.map_environment) : [for m in local.env_vars : lookup(m, "name")]
  env_vars_values      = var.map_environment != null ? values(var.map_environment) : [for m in local.env_vars : lookup(m, "value")]
  env_vars_as_map      = zipmap(local.env_vars_keys, local.env_vars_values)
  sorted_env_vars_keys = sort(local.env_vars_keys)

  sorted_environment_vars = [
    for key in local.sorted_env_vars_keys :
    {
      name  = key
      value = lookup(local.env_vars_as_map, key)
    }
  ]

  # This strange-looking variable is needed because terraform (currently) does not support explicit `null` in ternary operator,
  # so this does not work: final_environment_vars = length(local.sorted_environment_vars) > 0 ? local.sorted_environment_vars : null
  null_value = var.environment == null ? var.environment : null

  # https://www.terraform.io/docs/configuration/expressions.html#null
  final_environment_vars = length(local.sorted_environment_vars) > 0 ? local.sorted_environment_vars : local.null_value

  container_override = {
    name                 = var.name
    command              = var.command
    environment          = local.final_environment_vars
    cpu                  = var.cpu
    memory               = var.memory
    memoryReservation    = var.memory_reservation
    environmentFiles     = var.environment_files
    resourceRequirements = var.resource_requirements
  }

  container_override_without_null = {
    for k, v in local.container_override :
    k => v
    if v != null
  }
  json_map = jsonencode(merge(local.container_override_without_null, local.container_override))
}