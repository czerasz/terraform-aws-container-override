## [0.1.1](https://gitlab.com/czerasz/terraform-aws-container-override/compare/v0.1.0...v0.1.1) (2020-07-31)


### Code Refactoring

* improve documentation ([8fe1ecf](https://gitlab.com/czerasz/terraform-aws-container-override/commit/8fe1ecf420788d235e3a203ac121debf5c180df7))


### Continuous Integrations

* configure CI ([bb371a6](https://gitlab.com/czerasz/terraform-aws-container-override/commit/bb371a67b488adcb0bacf1028fffbe5c188c2dca))


### Documentation

* add documentation ([599b311](https://gitlab.com/czerasz/terraform-aws-container-override/commit/599b31172bb8d7fdac8e17d8694726bb252bce70))
