# Terraform AWS Container Override

Terraform module to abstract AWS container override.

## Usage

**IMPORTANT**: The `master` branch is used in `source` just as an example. In your code, do not pin to `master` because there may be breaking changes between releases. Instead pin to the release tag (e.g. `?ref=tags/x.y.z`) of one of our latest releases.

Basic usage:

```hcl
module "container_override" {
  source = "git::https://gitlab.com/czerasz/terraform-aws-container-override.git?ref=master"

  name    = "container_name"
  command = ["env"]
  map_environment = {
    "GENREATED_AT" : timestamp(),
  }
}

resource "aws_cloudwatch_event_target" "this" {
  target_id = var.target_id
  arn       = var.cluster_arn
  rule      = aws_cloudwatch_event_rule.this.name
  role_arn  = aws_iam_role.this.arn

  ecs_target {
    ...
  }

  input = "{ \"containerOverrides\": [ ${module.container_override.json_map_encoded} ]}"
}
```

<!-- BEGIN TERRAFORM DOCS -->
## Requirements

No requirements.

## Providers

No provider.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| command | The command that is passed to the container | `list(string)` | `null` | no |
| cpu | The number of cpu units reserved for the container, instead of the default value from the task definition. You must also specify a container name. | `number` | `null` | no |
| environment | The environment variables to pass to the container. This is a list of maps. map\_environment overrides environment | <pre>list(object({<br>    name  = string<br>    value = string<br>  }))</pre> | `[]` | no |
| environment\_files | One or more files containing the environment variables to pass to the container. This maps to the --env-file option to docker run. The file must be hosted in Amazon S3. This option is only available to tasks using the EC2 launch type. This is a list of maps | <pre>list(object({<br>    value = string<br>    type  = string<br>  }))</pre> | `null` | no |
| map\_environment | The environment variables to pass to the container. This is a map of string: {key: value}. map\_environment overrides environment | `map(string)` | `null` | no |
| memory | The hard limit (in MiB) of memory to present to the container, instead of the default value from the task definition. If your container attempts to exceed the memory specified here, the container is killed. You must also specify a container name. | `number` | `null` | no |
| memory\_reservation | The soft limit (in MiB) of memory to reserve for the container, instead of the default value from the task definition. You must also specify a container name. | `number` | `null` | no |
| name | The name of the container. Up to 255 characters ([a-z], [A-Z], [0-9], -, \_ allowed) | `string` | n/a | yes |
| resource\_requirements | The type and amount of a resource to assign to a container, instead of the default value from the task definition. The only supported resource is a GPU. | <pre>list(object({<br>    value = string<br>    type  = string<br>  }))</pre> | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| json\_map\_encoded | JSON string encoded container overrides for use with other terraform resources such as aws\_cloudwatch\_event\_target |
| json\_map\_encoded\_list | JSON string encoded list of container overrides for use with other terraform resources such as aws\_cloudwatch\_event\_target |
| json\_map\_object | JSON map encoded container overrides |


<!-- END TERRAFORM DOCS -->
