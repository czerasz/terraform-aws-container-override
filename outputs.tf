output "json_map_encoded_list" {
  description = "JSON string encoded list of container overrides for use with other terraform resources such as aws_cloudwatch_event_target"
  value       = "[${local.json_map}]"
}

output "json_map_encoded" {
  description = "JSON string encoded container overrides for use with other terraform resources such as aws_cloudwatch_event_target"
  value       = local.json_map
}

output "json_map_object" {
  description = "JSON map encoded container overrides"
  value       = jsondecode(local.json_map)
}